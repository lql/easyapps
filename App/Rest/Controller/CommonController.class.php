<?php
namespace Rest\Controller;
use Think\Controller;
class CommonController extends Controller {
    public $appid = 0,$appinfo,$srect;
    public $group, $ac, $mod,$minfo;
    public $navId = 0,$format = 'json';
    public function _initialize()
    {
    	$this->ac = ACTION_NAME;
        $this->mod = CONTROLLER_NAME;
        $this->group = GROUP_NAME;
        $this->assign('group', $this->group);
        $this->assign('mod', $this->mod);
        $this->assign('ac', $this->ac);
        $this->assign('thismod', $this->mod);

        if (I('format')) $this->format = I('format');


        $this->appid = I('appid');
        if (!$this->appid) exit();

    }

    public function index($id = 0,$page = 1,$limit = 10,$order = 'tid desc',$format = 'json')
    {
        if (!$id) return;
        $Nav = M('Nav');

        $nav = $Nav->find($id);
        if (!$nav) return;

        $model = D($this->mod);
        $model->app = $this->appinfo;
        if (!$model) return;

        $map['appid'] = $this->appid;
        $volist  = $model->_list_($nav,$map,$page,$limit);
        $volist['nav'] = $nav;
        $this->ajaxReturn($volist,$format);
    }

    

}