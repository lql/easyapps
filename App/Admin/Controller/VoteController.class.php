<?php
namespace Admin\Controller;
use Think\Controller;
class VoteController extends CommonController {
    public function index(){
    	$MagicFileds = M('MagicFileds');
    	$map['mid'] = $this->mid;
    	$map['appid'] = $this->appid;
    	$volist = $MagicFileds->where($map)->select();
        $this->assign('volist',$volist);
        $this->display();
    }
}