<?php
namespace Admin\Controller;
use Think\Controller;
class InfoController extends CommonController {
	public function index($p = 1,$navid = 0)
    {
        $Nav = D('Nav');
        $map['mid'] = $this->mid;
        $map['appid'] = $this->appid;
        $map['module'] = 'info';
        $navs = $Nav->where($map)->select();
        $this->assign('navs',$navs);

        unset($map['module']);
        if ($navid) 
            $map['navid'] = $navid;
        $Info = D('Info');
        $volist = $Info->where($map)->page($p,16)->select();
        $this->assign('volist',$volist);
        $this->assign('navid',$navid);
        $this->display();
    }

    public function insert()
    {
        $_POST['mid'] = $this->mid;
        $_POST['appid'] = $this->appid;
        $module = I('module');

        $Info = D('Info');
        if ( false === $Info->create() ) {
          $this->error( $Info->getError() );
        }
        //保存当前数据对象
        $InfoId = $Info->add();
        if ( $InfoId !== false ) { //保存成功
            redirect(U('Admin/Info/index'));
            // $this->success( '新增成功!', cookie( '_currentUrl_' ) );
        } else {
          //失败提示
          $this->error( '新增失败!' );
        }
    }

    public function add()
    {
        $Nav = D('Nav');
        $map['mid'] = $this->mid;
        $map['appid'] = $this->appid;
        $map['module'] = 'info';
        $navs = $Nav->where($map)->select();
        $this->assign('navs',$navs);
        $this->display();
    }

    public function edit($id = 0)
    {
        $Nav = D('Nav');
        $map['mid'] = $this->mid;
        $map['appid'] = $this->appid;
        $map['module'] = 'info';
        $navs = $Nav->where($map)->select();
        $this->assign('navs',$navs);

        $Info = D('Info');
        unset($map['module']);
        $map['id'] = $id;
        $vo = $Info->where($map)->find();
        $this->assign('vo',$vo);
        $this->display();
    }

    public function import()
    {
        $DB_DISCUZ = C("DB_DISCUZ");
        $this->assign('DB_DISCUZ',$DB_DISCUZ);
        $this->display();
    }

    function update($id = 0) 
    {
        $Info = D('Info');
        if ( false === $Info->create() ) {
          $this->error( $Info->getError() );
        }
        // 更新数据
        $map['mid'] = $this->mid;
        $map['appid'] = $this->appid;
        $map['id'] = $id;
        $infoId = $Info->where( $map )->save();
        if ( false !== $infoId ) {
          //成功提示
            redirect(U('Admin/Info/index'));
        } else {
          //错误提示
          $this->error( '编辑失败!' );
        }
    }

    public function doImport_dz()
    {
        $Nav = D('Nav');
        $ForumForum = D('ForumForum','Discuz');
        $map['type'] = 'forum';
        $map['status'] = 1;
        $navs = $ForumForum->where($map)->select();
        foreach ($navs as $k) {
            unset($map);
            $vo['pk'] = $k['fid'];
            $vo['name'] = $k['name'];
            $vo['module'] = 'discuz';
            $vo['mid'] = $this->mid;
            $vo['appid'] = $this->appid;

            $map['module'] = 'discuz';
            $map['pk'] = $k['fid'];
            $map['mid'] = $this->mid;
            $map['appid'] = $this->appid;
            $v = $Nav->where($map)->find();
            if (!$v) 
                $Nav->add($vo);
        }
        redirect(U('Admin/Nav/index'));
    }
}